from __future__ import absolute_import
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
import uvicorn
from api.database import mongo as db
from api.models.users import Users
from api.routes import root, users, sensor, condition, sensortype, core
from api.utils.hash import password_hash
from fastapi.staticfiles import StaticFiles
import os

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=["*"], allow_headers=["*"])
app.include_router(root.router, prefix='', tags=['root'])
app.include_router(users.router, prefix='/users', tags=['users'])
app.include_router(condition.router, prefix='/sensordata', tags=['sensordata'])
app.include_router(sensortype.router, prefix='/sensortype', tags=['sensortype'])
app.include_router(sensor.router, prefix='/sensor', tags=['sensor'])
app.include_router(core.router, prefix='/core', tags=['core'])

pathstatic = os.environ.get('API_DIR_STATIC', 'static')
if not os.path.exists(pathstatic):
    os.makedirs(pathstatic)
app.mount("/static", StaticFiles(directory=pathstatic), name="static")

if __name__ == "__main__":
    admin = Users(
        full_name='Administrator',
        username='admin',
        email='admin@api.com',
        password=password_hash('admin'),
        avatar=''
    )
    if db.users_db.find_one({'username': 'admin'}) is None:
        data = admin.dict()
        data['_id'] = db.getNextSequence(db.users_db)
        db.users_db.insert_one(data)
    else:
        print('User admin already exists')
    uvicorn.run(app, host='0.0.0.0', port=8000)
