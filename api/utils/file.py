import os
import time
from .hash import password_hash


def save_file_to_static(directory, file):
    path_static = os.environ.get('API_DIR_STATIC', 'static')

    extension = file.filename.split(".")
    filename = password_hash(str(time.time())) +"."+ extension[len(extension) - 1]

    directory_save = path_static + directory
    print(directory_save)
    if not os.path.exists(directory_save):
        os.makedirs(directory_save)
    path = os.path.join(directory_save, filename)

    f = open(path, 'w+b')
    contents = file.file.read()
    binary_format = bytearray(contents)
    f.write(binary_format)
    f.close()
    return path
