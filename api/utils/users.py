from api.database import mongo as db


def check_user(username):  # return True if user exists
    if db.users_db.find_one({'username': username}) is None:
        db.client.close()
        return False
    return True


def verify_user(username, password_hash):
    if db.users_db.find_one({'$and': [{'username': username}, {'password': password_hash}]}, {'_id': 0}) is None:
        db.client.close()
        return False
    return True
