from pydantic import BaseModel

class BaseResponse(BaseModel):
    status: str
    reason: str

class TokenExpired(BaseResponse):
    token: str


class UserError(BaseResponse):
    username: str
