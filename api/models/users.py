from pydantic import BaseModel
from typing import Optional, Literal


class Register(BaseModel):
    full_name: Optional[str] = None
    username: Optional[str] = None
    email: Optional[str] = None
    password: Optional[str] = None



class Token(BaseModel):
    access_token: Optional[str] = None
    token_type: Optional[str] = None


class Users(BaseModel):
    rfid: Optional[str] = None
    full_name: Optional[str] = None
    username: Optional[str] = None
    email: Optional[str] = None
    password: Optional[str] = None
    avatar: Optional[str] = None
    created_at: Optional[int] = None
    updated_at: Optional[int] = None



