from pydantic import BaseModel
from typing import Optional, Literal

class SensorType(BaseModel):
    name: Optional[str] = None
    created_at: Optional[int] = None
    updated_at: Optional[int] = None

class Sensor(BaseModel):
    name: str
    type_id: int
    created_at: Optional[int] = None
    updated_at: Optional[int] = None

class Condition(BaseModel):
    sensor_id: int
    value: float
    created_at: Optional[int] = None
    updated_at: Optional[int] = None