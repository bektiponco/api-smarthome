from fastapi import APIRouter, Depends, File, UploadFile
from jwt import PyJWTError
from ..models.response import TokenExpired
from ..models.users import Users
from ..utils.oauth2 import oauth2_scheme
from ..utils.token import verify_token
from ..utils.file import save_file_to_static
from ..database import mongo as db
from jwt import PyJWTError
from ..models.response import TokenExpired
import starlette.status as code


router = APIRouter()


@router.get("/")
async def read_users(token: str = Depends(oauth2_scheme)):
    return db.readDB(token, db.users_db)

@router.post("/insert")
async def insert(form: Users, token: str = Depends(oauth2_scheme)):
    return db.insertDB(token, db.users_db, form)

@router.post("/edit")
async def edit_users(id: int, form: Users, token: str = Depends(oauth2_scheme)):
    return db.updateDB(token, db.users_db, id, form)


