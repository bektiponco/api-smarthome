import datetime
from fastapi import APIRouter, Depends, File, UploadFile
from fastapi.security import OAuth2PasswordRequestForm
from ..database import mongo as db
from ..models.core import Condition
from ..utils.oauth2 import oauth2_scheme
from ..utils.file import save_file_to_static
import time
from ..utils.token import verify_token
from jwt import PyJWTError
from ..models.response import TokenExpired
import starlette.status as code
import datetime, calendar
import random

router = APIRouter()

@router.get("/read")
async def read(sensor_id: int):
    return db.readDB({}, db.condition_db, {"sensor_id": sensor_id})

@router.get("/readlamp")
async def read(sensor_id: int):
    jsonresult = db.readDB({}, db.condition_db, {"sensor_id": sensor_id})
    if (len(jsonresult["result"])==0):
        value = 0
    else:
        value = jsonresult["result"][0]
        value = value["value"]
    print(jsonresult)
    print(value)
    return value

@router.post("/updatedata")
async def updatedata(data: Condition):
    dataInsert = data
    if type(dataInsert) is not dict:
        dataInsert = dataInsert.dict()

    # dataInsert["_id"] = db.getNextSequence(db.condition_db)
    # dataInsert['created_at'] = time.time() * 1000
    dataInsert['updated_at'] = time.time() * 1000

    filter = {'sensor_id': dataInsert["sensor_id"]}
    update = {'$set': dataInsert}
    options = {'upsert': True}
    db.condition_db.update_one(filter, update, **options)
    return {'status': code.HTTP_200_OK, 'data': dataInsert}

@router.get("/updatedataget")
async def updatedataget(sensor_id: int, value: float):
    dataInsert = Condition(sensor_id=sensor_id, value=value)
    if type(dataInsert) is not dict:
        dataInsert = dataInsert.dict()

    # dataInsert["_id"] = db.getNextSequence(db.condition_db)
    # dataInsert['created_at'] = time.time() * 1000
    dataInsert['updated_at'] = time.time() * 1000

    filter = {'sensor_id': dataInsert["sensor_id"]}
    update = {'$set': dataInsert}
    options = {'upsert': True}
    db.condition_db.update_one(filter, update, **options)
    return {'status': code.HTTP_200_OK, 'data': dataInsert}

@router.get("/delete")
async def delete(id: int, token: str = Depends(oauth2_scheme)):
    return db.deleteDB(token, db.condition_db, id)

@router.post("/update")
async def update(id: int, form: Condition, token: str = Depends(oauth2_scheme)):
    return db.updateDB(token, db.condition_db, id, form)
