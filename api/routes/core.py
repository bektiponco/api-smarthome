import datetime
from fastapi import APIRouter, Depends, File, UploadFile
from fastapi.security import OAuth2PasswordRequestForm
from ..database import mongo as db
from ..models.core import Sensor
from ..utils.oauth2 import oauth2_scheme
from ..utils.file import save_file_to_static
from ..utils.token import verify_token
from jwt import PyJWTError
from ..models.response import TokenExpired
import starlette.status as code
import datetime, calendar
import random


router = APIRouter()


@router.get("/score")
async def score(date: datetime.date = datetime.date.today()):
    databersih = list(db.score_db.aggregate([
        {
            "$set": {
                "date": {"$toDate": "$updated_at"}
            }
        },
        {
            "$match": {
                "scoretype_id": 2
            }
        }
    ]))
    datakotor = list(db.score_db.aggregate([
        {
            "$set": {
                "date": {"$toDate": "$updated_at"}
            }
        },
        {
            "$match": {
                "scoretype_id": 1
            }
        }
    ]))
    # databersih = 0
    return {'status': code.HTTP_200_OK, 'result': {'databersih': databersih, 'datakotor': datakotor, 'persentase': len(databersih)/(len(databersih)+len(datakotor))*100}, }

