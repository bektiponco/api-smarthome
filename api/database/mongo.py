import os
from pymongo import MongoClient
import urllib.parse
import time
from ..utils.token import verify_token
from jwt import PyJWTError
from ..models.response import TokenExpired
import starlette.status as code


host = os.environ.get('API_DB_HOST', '103.150.197.78')
username = urllib.parse.quote_plus('seribucoding')
password = urllib.parse.quote_plus('seribucoding')

client = MongoClient(host=host, username=username,password=password, authSource='admin',authMechanism='SCRAM-SHA-1')
api_collection = client.apismarthome

users_db = api_collection.api_users
token_db = api_collection.api_token
condition_db = api_collection.api_condition
sensor_db = api_collection.api_sensor
sensortype_db = api_collection.api_sensortype


def getNextSequence(collection):
    data = list(collection.find())
    if len(data) == 0:
        return 1;
    else:
        data = list(collection.find().sort('_id', -1).limit(1))[0]
        return data['_id'] + 1;

def insertDB(token, collection, data):
    try:
        verify_token(token)
        dataInsert = data
        if type(dataInsert) is not dict:
            dataInsert = dataInsert.dict()

        dataInsert["_id"] = getNextSequence(collection)
        dataInsert['created_at'] = time.time()*1000
        dataInsert['updated_at'] = time.time()*1000
        collection.insert_one(dataInsert)

        return {'status': code.HTTP_200_OK, 'data':dataInsert}
    except PyJWTError:
        return TokenExpired(
            status='failed',
            reason='token expired',
            token=token
        )

def updateDB(token, collection, _id, data):
    try:
        verify_token(token)
        dataInsert = data
        if type(dataInsert) is not dict:
            dataInsert = dataInsert.dict()

        dataInsert['updated_at'] = time.time()*1000
        dataInsert = {k: v for k, v in dataInsert.items() if v is not None and v is not []}

        if "_id" in dataInsert:
            del dataInsert["_id"]

        collection.update_one({'_id': _id}, {'$set': dataInsert})
        client.close()
        return {'status': code.HTTP_200_OK, 'data':dataInsert}
    except PyJWTError:
        return TokenExpired(
            status='failed',
            reason='token expired',
            token=token
        )


def readDB(token, collection, filter = {}):
    try:
        if token!={}:
            verify_token(token)

        result = list(collection.find(filter))
        return {'status': code.HTTP_200_OK, 'result': result}
    except PyJWTError:
        return TokenExpired(
            status='failed',
            reason='token expired',
            token=token
        )

def deleteDB(token, collection, _id):
    try:
        verify_token(token)
        collection.delete_one({'_id': _id})
        client.close()
        return {'status': code.HTTP_200_OK, 'data':_id}
    except PyJWTError:
        return TokenExpired(
            status='failed',
            reason='token expired',
            token=token
        )