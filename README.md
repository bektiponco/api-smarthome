API rfid features:

- Based on FastAPI (obviously)
- Structured into (sub)modules for easy large projects
- Using MongoDB (more in future)
- Easy to add custom data models
- Typing with Pydantic
- Deployment using Uvicorn
- Token-based authentication using PyJWT
- Testing using Pytest
- Code coverage report using pytest-cov
